# Pac-Man

Un projet de fin d'année de Terminale NSI :

- POO
- Graphes et parcours de graphes

L'algorithme de poursuite des fantômes est une application directe d'un parcours BFS appliqué au calcul d'une distance à un sommet donné, en nombre de "sauts" de case...donc plus simple à coder qu'un A* ou un Dijkstra :-)...

# A améliorer :

- Découpage en module pour chacune des classes ( et donc travail sur le thème "Modularité" du programme ;-)
- Amélioration de l'affichage
- Fonctionnaluités supplémentaires : score, mode "panique" des fantômes à implémenter complètement
- ....