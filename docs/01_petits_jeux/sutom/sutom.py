"""
sutom.py

Définit une classe Sutom pour assister le joueur dans sa partie de
sutom.nocle.fr

Utilisation : dans un interprete interactif, on charge le script. On initialise un objet Sutom avec le motif du jour : j = Sutom('r*******') par exemple

Dès lors on peut faire print(j) pour avoir une idée du nombre de mots possibles ou même la totalités des most s'il n'y en a pas trop

puis j.dialogue() permet de saisir sa proposition de mot et la réponse du jeu
A tout moment j.mots donne l'ensemble des mots possibles, j.apparition le nombre d'apparition de lettres dans le mot, j.pos_interdites les positions interdites.
"""
import random

class Sutom:
    
    # DIC_FILE = 'mots_avec_et_sans_accents.dic'
    DIC_FILE = 'sans_accent.dic'
    ENCODING = 'utf-8'
    SEUIL = 20 # si le nb de mots admissibles est < seuil on affiche tous les mots
    JOKER = '*'
    ALPHABET = 'abcdefghijklmnopqrstuvwxyz'
    
    # codes réponses
    #
    # BIEN_PLACEE = '◼'
    # MAL_PLACEE = '●'
    # ABSENTE = '◻'
    
    BIEN_PLACEE = '1'
    MAL_PLACEE = '2'
    ABSENTE = '0'
    
    def __init__(self, motif):
        self.motif = list(motif.lower()) # les mots du dictionnaire sont en minuscules
        self.taille = len(motif)
        self.mots = set()        # ensemble des mots admissibles 
        self.dic = set()         # ensemble des mots du dictionnaire
        self.apparition = {}     # comptabilise avec certitude le nombre d'apparition d'une lettre dans le mot mystere
        self.pos_interdites = {} # dictionnaire des positions interdites par lettre
        self.load()
                
    def __str__(self):
        nb_mots = len(self.mots)
        if self.end():
            return f"Gagné, le mot mystère était : {''.join(self.motif).upper()}"
        elif nb_mots < Sutom.SEUIL:
            return '\n'.join(self.mots)
        else:
            return f'{nb_mots} possibilités...'
        
    def load(self):
        """Charge le dictionnaire et initialise les mots admissibles"""
        with open(Sutom.DIC_FILE, 'r', encoding=Sutom.ENCODING) as datas:
            for line in datas:
                self.dic.add(line.strip())
        self.mots = {mot for mot in self.dic if self.match(mot) and not self.interdit(mot)}
    
    def end(self):
        return Sutom.JOKER not in self.motif
    
    def position_interdite(self, lettre, pos):
        """renvoie True ssi pos est une position identifée comme interdite pour lettre"""
        return lettre in self.pos_interdites and pos in self.pos_interdites[lettre]
    
    def apparait_trop(self, lettre, nb_fois):
        """renvoie True ssi nb_fois c'est trop d'apparition pour lettre au regard des infos connues"""
        return lettre in self.apparition and nb_fois > self.apparition[lettre]
    
    def lettre_manquante(self, decompte):
        """les clés du dictionnaire pos_interdites sont des lettres qui sont présentes 
        dans le mot mystere... si cette lettre n'apparait pas dans le decompte des lettres
        du mot saisie c'est que la lettre manque et le mot sera donc rejeté"""
        return any(decompte[lettre] == 0 for lettre in self.pos_interdites) 

    def interdit(self, mot):
        """Renvoie True ssi
            - une lettre du mot apparait trop souvent ou 
            - une lettre est présente à une position où elle a été signifiée mal placée ou enfin
            - une lettre mal placée n'est pas du tout présente dans le mot
        """
        dans_le_mot = {lettre:0 for lettre in Sutom.ALPHABET}
        for pos, lettre in enumerate(mot):
            dans_le_mot[lettre] += 1
            if self.apparait_trop(lettre, dans_le_mot[lettre]):
                return True
            if self.position_interdite(lettre, pos):
                return True
        # à ce stade il n'y pas de lettre en trop ni de positions interdites
        # si une lettre manque le mot sera interdit sinon c'est ok
        return self.lettre_manquante(dans_le_mot)

    def match(self, mot):
        """Renvoie True ssi le mot mot coïncide avec le motif courant"""
        return len(mot) == self.taille and all((self.motif[i] == Sutom.JOKER) or (self.motif[i] == mot[i]) for i in range(len(mot)))        
                
    def update(self, saisie, reponse):
        """Met à jour la liste des mots admissibles avec les contraintes courantes"""
        absentes = set()
        dans_saisie = {} # pour compter les apparitions dans la proposition du joueur
        for i in range(self.taille):
            lettre, rep = saisie[i], reponse[i]
            if rep == Sutom.BIEN_PLACEE:
                self.motif[i] = lettre
                dans_saisie[lettre] = dans_saisie.get(lettre, 0) + 1
            elif rep == Sutom.MAL_PLACEE:
                self.pos_interdites[lettre] = self.pos_interdites.get(lettre, set()) | {i}
                dans_saisie[lettre] = dans_saisie.get(lettre, 0) + 1
            elif rep == Sutom.ABSENTE:
                absentes.add(lettre)
        for lettre in absentes:
            if lettre not in dans_saisie:
                self.apparition[lettre] = 0
            else:
                self.apparition[lettre] = dans_saisie[lettre]
        self.mots = {mot for mot in self.mots if self.match(mot) and not self.interdit(mot)}
    
    def suggestion(self):
        l_sugg = [mot for mot in self.mots if all(mot.count(lettre) == 1 for lettre in mot)]
        nb = min(len(l_sugg), Sutom.SEUIL)
        random.shuffle(l_sugg)
        for i in range(nb):
            print(l_sugg[i])
        
    def dialogue(self):
        saisie = input('Votre proposition  : ')
        reponse = input("La réponse de l'IA : ")
        self.update(saisie, reponse)
        print(self)

# def main():
#     motif = input(f'Donnez le motif du jour (utilisez {Sutom.JOKER} pour le joker): ')
#     puzzle = Sutom(motif)
#     puzzle.help()
#     while not puzzle.end():
#         puzzle.dialogue()

# if __name__ == '__main__':
#     main()
